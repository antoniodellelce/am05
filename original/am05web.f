c $Header:$
c**********************************************************************
c Armiento Mattsson AM05 functional for exchange and correlation, 
c non spin polarized.
c
c Version: 6 (nonspin, web)
c
c The latest version of this subroutine file is available at
c   http://dft.sandia.gov/functionals/AM05.html
c
c
c Usage:
c
c Below follows first a number of usage examples on how to calculate energy
c and potential in codes that use different schemes for calculating
c the potential, and therefore uses different input quantities. After the
c usage examples follows the main am05 routine.
c
c We include examples for the following schemes:
c
c   am05trad : The traditional scheme used e.g. by PRB 33, 8800 (1986).
c   am05wb : The White and Bird scheme [PRB 50, 4954 (1994)].
c   am05pgj : The Pople, Gill, and Johnson scheme [CPL 199, 557 (1992)].
c   am05wbnum : The White and Bird scheme with numerical derivatives.
c
c
c Citation request: 
c
c When using this functional, please cite:
c "R. Armiento and A. E. Mattsson, PRB 72, 085108 (2005);"
c
c
c License and copyright:
c
c (c) Rickard Armiento 2005-2008
c
c Permission is hereby granted, free of charge, to any person obtaining 
c a copy of this software and associated documentation files (the 
c "Software"), to deal in the Software without restriction, including 
c without limitation the rights to use, copy, modify, merge, publish, 
c distribute, sublicense, and/or sell copies of the Software, and to 
c permit persons to whom the Software is furnished to do so, subject 
c to the following conditions:
c
c The above copyright notice and this permission notice shall be 
c included in all copies or substantial portions of the Software.
c
c THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
c EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
c OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
c NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
c HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
c WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
c FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
c OTHER DEALINGS IN THE SOFTWARE.
c
c**********************************************************************
c
c
c**********************************************************************
c am05trad
c Usage example for traditional scheme, with example of stress output.
c
c input
c   n       electron density [bohr**(-3)]
c   gn      abs of gradient of density, |grad(n)| [bohr**(-4)]
c   gnggn   grad(n) * grad(|grad(n)|) [bohr**(-9)]
c   ln      laplacian of density, grad*grad(n)=grad**2(n) [bohr**(-5)]
c
c output
c   exc          exchange-correlation energy per electron [hartree]
c   vxc          exchange-correlation potential [hartree]
c   dfxcdgnogn   d(n*exc)/d(|grad(n)|) / |grad(n)|, for use in the
c                stress tensor [hartree * bohr**(5)]
c
c**********************************************************************

      subroutine am05trad(n,gn,gnggn,ln,exc,vxc,dfxcdgnogn)

      implicit none

c     ** Input parameters
      real*8 n, gn, gnggn, ln 

c     ** Output parameters
      real*8 exc, vxc, dfxcdgnogn

c     ** Internal parameters
      real*8 kF, s, u, t
      real*8 ex,ec,vx,vc
      real*8 dfxdgnogn,dfcdgnogn
      integer pot
      real*8 pi
      parameter (pi = 3.141592653589793238462643383279502884197d0)

c     ** Dummy parameters (not used)
      real*8 dfxdn,dfcdn

      kF = (3.0d0*pi**2*n)**(1.0d0/3.0d0)
      s = gn / (2.0d0*kF*n)  
      u = gnggn / ((2.0d0*kF)**3*n**2)
      t = ln / ((2.0d0*kF)**2*n) 

      pot = 1 
c
      call am05(n,s,u,t,ex,ec,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
c
c     ** Form the output
      exc = ex + ec
      vxc = vx + vc
      dfxcdgnogn = dfxdgnogn + dfcdgnogn

      return
      end


c**********************************************************************
c am05wb
c Usage example for White and Bird scheme [PRB 50, 4954 (1994)].
c
c input
c   n       electron density [bohr**(-3)]
c   gn      abs of gradient of density, |grad(n)| [bohr**(-4)]
c
c output
c   exc          exchange-correlation energy per electron [hartree]
c   dfxcdn       d(n*exc)/dn [hartree]
c   dfxcdgn      d(n*exc)/d(|grad(n)|) [hartree * bohr] 
c
c**********************************************************************

      subroutine am05wb(n,gn,exc,dfxcdn,dfxcdgn)

      implicit none

c     ** Input parameters
      real*8 n, gn

c     ** Output parameters
      real*8 exc, dfxcdn, dfxcdgn

c     ** Internal parameters
      real*8 kF, s
      real*8 ex,ec
      real*8 dfxdn,dfcdn
      real*8 dfxdgnogn,dfcdgnogn
      integer pot
      real*8 pi
      parameter (pi = 3.141592653589793238462643383279502884197d0)

c     ** Dummy parameters (not used)
      real*8 u, t, vx, vc

      kF = (3.0d0*pi**2*n)**(1.0d0/3.0d0)
      s = gn / (2.0d0*kF*n)  

      pot = 2 
c
      call am05(n,s,u,t,ex,ec,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
c
c     ** Form the output
      exc = ex + ec
      dfxcdn = dfxdn + dfcdn
      dfxcdgn = gn*(dfxdgnogn + dfcdgnogn)

      return
      end


c**********************************************************************
c am05pgj
c Usage example for Pople, Gill, and Johnson scheme 
c [Chem Phys Lett 199, 557 (1992)].
c
c input
c   na      electron spin-up density [bohr**(-3)]
c   nb      electron spin-down density [bohr**(-3)]
c   gamaa   abs squared of gradient of spin-up density, 
c           |grad(na)|**2 [bohr**(-8)]
c   gambb   abs squared of gradient of spin-down density, 
c           |grad(nb)|**2 [bohr**(-8)]
c   gamab   gradient of spin-up density * gradient of spin-down density, 
c           grad(na)*grad(nb) [bohr**(-8)]
c
c output
c   fxc       exchange-correlation energy density [hartree * bohr**(-3)]
c   dfxcdna   d(fxc)/dna  [hartree]
c   dfxcdnb   d(fxc)/dnb  [hartree]
c   dfxcdgaa  d(fxc)/d(|grad(na)|**2)  [hartree * bohr**(5)]
c   dfxcdgbb  d(fxc)/d(|grad(nb)|**2)  [hartree * bohr**(5)]
c   dfxcdgab  d(fxc)/d(grad(na)*grad(nb))   [hartree * bohr**(5)]
c
c Note that 
c d(fxc)/d(|grad(n)|**2) = 1/2/|grad(n)|*d(fxc)/d(|grad(n)|).
c
c Note also that this version of AM05 is not spin-resolved
c and output quantities thus correspond to the total density, 
c n=na+nb, and the abs of the gradient of the total density, 
c |grad(n)|**2 = |grad(na) + grad(nb)|**2 = gamaa+gambb+2*gamab. 
c
c**********************************************************************

      subroutine am05pgj(na,nb,gamaa,gambb,gamab,fxc,
     *                   dfxcdna,dfxcdnb,dfxcdgaa,dfxcdgab,dfxcdgbb)

      implicit none

c     ** Input parameters
      real*8 na, nb, gamaa, gambb, gamab

c     ** Output parameters
      real*8 fxc, dfxcdna, dfxcdnb, dfxcdgaa, dfxcdgab, dfxcdgbb

c     ** Internal parameters
      real*8 n, gn, kF, s
      real*8 ex,ec
      real*8 dfxdn,dfcdn
      real*8 dfxdgnogn,dfcdgnogn
      integer pot
      real*8 pi
      parameter (pi = 3.141592653589793238462643383279502884197d0)

c     ** Dummy parameters (not used)
      real*8 u, t, vx, vc

      n = na + nb
      gn = (gamaa + gambb + 2.0d0*gamab)**(1.0d0/2.0d0)
      kF = (3.0d0*pi**2*n)**(1.0d0/3.0d0)
      s = gn / (2.0d0*kF*n)  

      pot = 2 
c
      call am05(n,s,u,t,ex,ec,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
c
c     ** Form the output
      fxc = n*(ex + ec)
      dfxcdna = dfxdn + dfcdn
      dfxcdnb = dfxdn + dfcdn
      dfxcdgaa = (dfxdgnogn + dfcdgnogn)/2.0d0
      dfxcdgbb = (dfxdgnogn + dfcdgnogn)/2.0d0
      dfxcdgab = dfxdgnogn + dfcdgnogn
      return
      end
      
c**********************************************************************
c am05wbnum
c Usage example for White and Bird scheme [PRB 50, 4954 (1994)] with
c numerical derivatives. 
c
c This should not be needed when analytical derivatives are available
c as they are for AM05. But this is a good way to check that the
c analytical derivatives are actually right. So, if you do changes
c inside the main am05 subroutine this might be a good way to check them.
c
c input
c   n       electron density [bohr**(-3)]
c   gn      abs of gradient of density, |grad(n)| [bohr**(-4)]
c
c output
c   exc          exchange-correlation energy per electron [hartree]
c   dfxcdn       d(n*exc)/dn [hartree]
c   dfxcdgn      d(n*exc)/d(|grad(n)|) [hartree * bohr]
c
c**********************************************************************

      subroutine am05wbnum(n,gn,exc,dfxcdn,dfxcdgn)

      implicit none

c     ** Input parameters
      real*8 n, gn

c     ** Output parameters
      real*8 exc, dfxcdn, dfxcdgn

c     ** Internal parameters
      real*8 kF, s, delta, sdelta
      real*8 nmd, npd, kFnmd, kFnpd, snmd, snpd, sgnmd, sgnpd
      real*8 ex, ec
      real*8 exnmd, ecnmd, exnpd, ecnpd, exgnmd, ecgnmd, exgnpd, ecgnpd
      integer pot
      real*8 pi
      parameter (pi = 3.141592653589793238462643383279502884197d0)

c     ** Dummy parameters (not used)
      real*8 u, t, vx, vc
      real*8 dfxdn,dfcdn
      real*8 dfxdgnogn,dfcdgnogn

      kF = (3.0d0*pi**2*n)**(1.0d0/3.0d0)
      s = gn / (2.0d0*kF*n)  
      delta = n/100.0d0
      if (delta .ge. 1.0e-4) delta = 1.0e-4

      nmd = n - delta
      npd = n + delta
      kFnmd = (3.0d0*pi**2*(nmd))**(1.0d0/3.0d0)
      kFnpd = (3.0d0*pi**2*(npd))**(1.0d0/3.0d0)
      snmd = gn / (2.0d0*kFnmd*nmd)
      snpd = gn / (2.0d0*kFnpd*npd)

      sdelta = gn/100.0d0
      if (sdelta .ge. 1.0e-4) sdelta = 1.0e-4
      sgnmd = (gn - sdelta)/ (2.0d0*kF*n)
      sgnpd = (gn + sdelta)/ (2.0d0*kF*n)

      pot = 0 

      call am05(n,s,u,t,ex,ec,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
      call am05(nmd,snmd,u,t,exnmd,ecnmd,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
      call am05(npd,snpd,u,t,exnpd,ecnpd,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
      call am05(n,sgnmd,u,t,exgnmd,ecgnmd,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)
      call am05(n,sgnpd,u,t,exgnpd,ecgnpd,vx,vc,
     *                dfxdn,dfcdn,dfxdgnogn,dfcdgnogn,pot)

c     ** Form the output
      exc = ex + ec
      dfxcdn = (npd*(exnpd + ecnpd) - nmd*(exnmd + ecnmd))/(2.0d0*delta)
      dfxcdgn = n*((exgnpd + ecgnpd) - (exgnmd + ecgnmd))/(2.0d0*sdelta)

      return
      end

c**********************************************************************
c am05
c Generic non-spin routine for Armiento Mattsson AM05 functional for 
c exchange and correlation.
c
c input
c   n        electron density [bohr**(-3)]
c   dn       abs of gradient of density (n) [bohr**(-4)]
c   u        scaled grad n * grad | grad n |
c   t        scaled laplacian of density
c   pot      integer: 
c              2 = calculate quantities for the White and Bird,
c                  and the Pople, Gill, and Johnson schemes for
c                  potentials (u and t are never touched
c                  and vx and vc give undefined output)
c              1 = calculate potential in the traditional scheme
c                  (all input needed and all output well defined)
c              0 = don't calculate potential (u and t are never 
c                  touched and only ex and ec output are well defined)
c
c For exact definitions of s, u, and t, see PRB 33, 8800 (1986)
c or the documentation associated to this subroutine available at
c http://dft.sandia.gov/functionals/AM05.html.
c
c output
c   ex          exchange energy per electron [hartree]
c   ec          correlation energy per electron [hartree]
c   vx          exchange potential [hartree]
c   vc          correlation potential [hartree]
c   dfxdn       d(n*ex)/dn [hartree]
c   dfcdn       d(n*ec)/dn [hartree]
c   dfxddnodn   1/|grad(n)| * d(n*ex)/d(|grad(n)|) [hartree * bohr**(5)]
c   dfcddnodn   1/|grad(n)| * d(n*ec)/d(|grad(n)|) [hartree * bohr**(5)]
c
c Citation request: when using this functional, please cite:
c "R. Armiento and A. E. Mattsson, PRB 72, 085108 (2005). " 
c
c (Equation numbers cited below refers to this article.)
c
c**********************************************************************

      subroutine am05(n,s,u,t,ex,ec,vx,vc,
     *                dfxdn,dfcdn,dfxddnodn,dfcddnodn,pot)
      implicit none

c     ** Input parameters
      real*8 n, s, u, t 
      integer pot

c     ** Output parameters
      real*8 ex, ec, vx, vc
      real*8 dfxdn, dfcdn, dfxddnodn, dfcddnodn

c     ** Constants and shorthand notation. 
c     **  a and g from Eq. (11), c from below Eq. (8).
      real*8 pi, g, a, c, s2, kF
      parameter (pi = 3.141592653589793238462643383279502884197d0)
      parameter (g = 0.8098d0, a = 2.804d0)
      parameter (c = 0.7168d0)

c     ** Local variables
      real*8 exlda, vxlda, eclda, vclda, X, Xsos, Xsossos
      real*8 Hx, Hxsos, Hxsossos, Hc, Hcsos, Hcsossos
      real*8 F, Fsos, s2Fsossos
      real*8 szsoz, mixder
      real*8 denom, denomsos, sdenomsoss
      real*8 zfac, zosn, w

c     ** Cutoff 
      if((n .le. 1.0d-16)) then
         ex = 0.0d0
         ec = 0.0d0
         vx = 0.0d0
         vc = 0.0d0
         dfxdn = 0.0d0
         dfcdn = 0.0d0
         dfxddnodn = 0.0d0
         dfcddnodn = 0.0d0
         return
      endif
      
      s2 = s**2

c     *******************
c       LDA correlation
c     *******************
      call am05_ldapwc(n,eclda,vclda)

c     *******************
c        LDA exchange
c     *******************
      call am05_ldax(n,exlda,vxlda)
      
c     ********************
c          Exchange
c     ********************

c     ** Interpolation index Eq. (9)
      X = 1.0d0/(1.0d0 + a*s2)
      
c     ** Airy LAA refinement function Eq. (8)
      call am05_labertw(s**(3.0d0/2.0d0)/sqrt(24.0d0),w)

c     ** am05_lambertw give back argument if it is < 1.0e-20
c     ** (1.0e-14)^{3/2} = 1.0e-21 => give  low s limit for z/s
c     ** zosn = normalized z/s
      if (s < 1.0e-14) then
              zosn = 1.0d0
      else
              zosn = 24.0d0**(1.0d0/3.0d0)*w**(2.0d0/3.0d0)/s
      end if
      zfac = s2*(zosn*27.0d0/32.0d0/pi**2)**2

c     ** denom = denominator of Airy LAA refinement function
      denom = 1.0d0 + c*s2*zosn*(1.0d0 + zfac)**(1.0d0/4.0d0)
      F = (c*s2 + 1.0d0)/denom
      
c     ** Exchange refinement function. Eq. (12)
      Hx = X + (1.0d0 - X)*F

c     ** Exchange energy per particle, Eq. (12), Ex = Integrate[n*ex]
      ex = exlda*Hx
      
c     ********************
c         Correlation
c     ********************
      
c     ** Correlation refinement function. Eq. (12) 
      Hc = X + g*(1.0d0 - X)
      
c     ** Correlation energy per particle, Eq. (12), Ec = Integrate[n*ec]
      ec = eclda*Hc

      if (pot .eq. 0) return

      kF = (3.0d0*pi**2*n)**(1.0d0/3.0d0)

c     ***************************
c         Exchange derivatives for White and Bird and 
c         Pople, Gill, and Johnson schemes
c     ***************************

c     ** Interpolation index derivatives: 1/s dX/ds
      Xsos = -2.0d0*a*X**2

c     ** Airy LAA refinement function derivatives, 1/s dF/ds 
c     ** szsoz = s*(dz/ds)/z
      szsoz = 1.0d0/(1.0d0 + w)

      Fsos = c/denom**2*(2.0d0 - zosn*
     *            ((1.0d0 - c*s2)*(1.0d0 + zfac)**(1.0d0/4.0d0) +
     *             (1.0d0 + c*s2)*(1.0d0 + 3.0d0/2.0d0*zfac)/
     *                      (1.0d0 + zfac)**(3.0d0/4.0d0)*szsoz))

c     ** Refinement function derivatives, 1/s dHx/ds
c     ** We use that (1 - X) = a*X*s2
      Hxsos = (1.0d0 - X)*Fsos - (F - 1.0d0)*Xsos

      dfxdn = vxlda*Hx - 4.0d0/3.0d0*exlda*s2*Hxsos
      dfxddnodn = exlda*Hxsos/((2.0d0*kF)**2*n)

c     *****************************
c        Correlation derivatives for White and Bird and 
c        Pople, Gill, and Johnson schemes
c     *****************************
      
c     ** Correlation refinement function derivatives, 1/s dF/ds 
      Hcsos = Xsos*(1.0d0 - g)

      dfcdn = vclda*Hc - 4.0d0/3.0d0*eclda*s2*Hcsos
      dfcddnodn = eclda*Hcsos/((2.0d0*kF)**2*n)

      if (pot .eq. 2) return

c     ***************************
c         Exchange potential in traditional scheme
c     ***************************

c     ** Interpolation index derivatives: 1/s d/ds(1/s dX/ds)                   
      Xsossos = 8.0d0*a**2*X**3

c     ** Airy LAA refinement function derivatives s^2 1/s d/ds (1/s dF/ds) 
c     **  mixder = szsoz + s^2*(d^2z/ds^2)/z
      mixder = (2.0d0 - w)/(2.0d0*(1.0d0 + w)**3)

c     ** denomsos = 1/s d(denom)/ds,  sdenomsoss = s*d/ds(1/s d(denom)/ds))
      denomsos = c*zosn/(1.0d0 + zfac)**(3.0d0/4.0d0)*
     *     (1.0d0 + zfac + (1.0d0 + 3.0d0/2.0d0*zfac)*szsoz)

      sdenomsoss = c*zosn/(1.0d0 + zfac)**(7.0d0/4.0d0)*
     *     (-1.0d0 - zfac*(2.0d0 + zfac)
     *      + (1.0d0 + zfac/2.0d0*(5.0d0 + 3.0d0*zfac))*mixder
     *      + 3.0d0/2.0d0*zfac*(1.0d0 + zfac/2.0d0)*szsoz**2)

      s2Fsossos = (-4.0d0*c*s2*denom*denomsos + (c*s2 + 1.0d0)*
     *                    (2.0d0*s2*denomsos**2 - denom*sdenomsoss))/
     *                      denom**3

c     ** Refinement function derivatives 1/s d/ds (1/s dHx/ds) 
c     ** We use that (1 - X) = a*X*s2
      Hxsossos = - 2.0d0*Fsos*Xsos + a*X*s2Fsossos -
     *                  (F - 1.0d0)*Xsossos

c     ** vx formula for gradient dependent functional,
      vx = vxlda*(Hx - s2*Hxsos) +
     *     exlda*((4.0d0/3.0d0*s2-t)*Hxsos - 
     *     (u-4.0d0/3.0d0*s**3)*s*Hxsossos)

c     *****************************
c        Correlation potential in traditional scheme
c     *****************************
      
c     ** Correlation refinement function derivatives, 1/s d/ds (1/s dHc/ds)
      Hcsossos = Xsossos*(1.0d0 - g)
      
c     ** vc formula for gradient dependent functional,
      vc = vclda*(Hc - s2*Hcsos) +
     *     eclda*((4.0d0/3.0d0*s2 - t)*Hcsos - 
     *     (u - 4.0d0/3.0d0*s**3)*s*Hcsossos)

      return
      end

c     ******************************************
c       Local density approximation exchange
c
c       input
c       n        electron density [bohr**(-3)]
c
c       output
c       ex       exchange energy per electron [hartree]
c       vx       exchange potential [hartree]
c
c       Copyright (c) 2005, Rickard Armiento
c       All rights reserved.
c     ******************************************

      subroutine am05_ldax(n,ex,vx)
c     ** Input parameters
      real*8 n

c     ** Output parameters
      real*8 ex, vx

c     ** Constants
      real*8 pi
      parameter (pi = 3.141592653589793238462643383279502884197d0)

      vx = -(3.0d0*n/pi)**(1.0d0/3.0d0)
      ex = (3.0d0/4.0d0*vx)

      return
      end

c     ***********************************************
c       Local density approximation correlation
c
c       input
c       n        electron density [bohr**(-3)]
c
c       output
c       ec       correlation energy per electron [hartree]
c       vc       correlation potential [hartree]
c
c       As parameterized by Perdew Wang,
c         Phys. Rev. B 45, 13244 (1992) 
c       Based on monte carlo data by Ceperley Alder, 
c         Phys. Rev. Lett. 45, 566 (1980)
c
c       (Clean room implementation from paper)
c
c       Copyright (c) 2005, Rickard Armiento
c       All rights reserved.
c     ***********************************************
      subroutine am05_ldapwc(n,ec,vc)
      implicit none
c     ** Input parameters
      real*8 n

c     ** Output parameters
      real*8 ec, vc

c     ** Constants
      real*8 pi
      real*8 A0,a01,b01,b02,b03,b04
      parameter (pi = 3.141592653589793238462643383279502884197d0)
      parameter (a01 = 0.21370d0)
      parameter (b01 = 7.5957d0)
      parameter (b02 = 3.5876d0)
      parameter (b03 = 1.6382d0)
      parameter (b04 = 0.49294d0)
c     ** Paper actually use this:
c      parameter (A0 = 0.031091d0)
c     ** But routines now "defacto standard" was distributed using:
      parameter (A0 = 0.0310907d0)

c     ** Local variables
      real*8 rsq
      real*8 Q0, Q1, Q1p, ecrs

      rsq = (3.0d0/(4.0d0*pi*n))**(1.0d0/6.0d0)

      ec = -2.0d0*A0*(1.0d0 + a01*rsq**2)* 
     *     log(1.0d0 + 1.0d0/
     *     (2.0d0*A0*rsq*(b01 + rsq*(b02 + rsq*(b03 + b04*rsq)))))

      Q0 = -2.0d0*A0*(1.0d0 + a01*rsq**2)
      Q1 = 2.0d0*A0*rsq*(b01 + rsq*(b02 + rsq*(b03 + b04*rsq)))
      Q1p = A0*(b01/rsq+2.0d0*b02+3.0d0*b03*rsq+4.0d0*b04*rsq**2)
      ecrs = -2.0d0*A0*a01*log(1.0d0 + 1.0d0/Q1)-Q0*Q1p/(Q1**2+Q1)

      vc=ec - rsq**2/3.0d0*ecrs
      
      end

c     ***********************************************
c       LambertW function. 
c
c       Corless, Gonnet, Hare, Jeffrey, and Knuth (1996), 
c         Adv. in Comp. Math. 5(4):329-359. 
c       Implementation approach loosely inspired by the 
c       GNU Octave version by N. N. Schraudolph, but this 
c       implementation is only for real values and 
c       principal branch.
c
c       Copyright (c) 2005, Rickard Armiento
c       All rights reserved.
c     ***********************************************

      subroutine am05_labertw(z,result)
      implicit none

c     input
      real*8 z
c     output
      real*8 result
c     local variables
      real*8 e,t,p    
      integer i

c     ** If z too low, go with the first term of the power expansion, z
      if( z .lt. 1.0d-20) then
         result = z
         return
      endif

      e = exp(1.0d0)

c     ** Inital guess
      if( abs(z + 1.0d0/e) .gt. 1.45d0 ) then
c        ** Asymptotic expansion at 0 and Inf
         result = log(z)
         result = result - log(result)
      else
c        ** Series expansion about -1/e to first order
         result = 1.0d0*sqrt(2.0d0*e*z + 2.0d0) - 1.0d0
      endif

c     ** Find result through iteration
      do i=1,10
         p = exp(result)
         t = result*p - z
         if( result .ne. -1.0d0 ) then
            t = t/(p*(result + 1.0d0) - 
     *           0.5d0*(result + 2.0d0)*t/(result + 1.0d0))
         else
            t = 0.0d0
         endif
         result = result - t
         if(abs(t) < (2.48d0*1.0d-14)*(1.0d0 + abs(result))) then
            return
         endif
      enddo
c     ** This should never happen!;
      write(0,*) 'am05_labertw: iteration limit reached.' 
      write(0,*) 'Likely caused by invalid input, execution aborted.'
      stop
      end


