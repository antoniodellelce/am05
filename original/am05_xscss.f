c**********************************************************************
c Armiento Mattsson am05 energy functional for exchange and correlation 
c
c Spin polarization type: exchange index scales with the separate 
c spin densities; correlation index and prefactor both scales with
c the separate spin densities. (xscss)
c
c Version: 4_xscss
c
c input
c   n(2)     electron upspin/downspin density [bohr**(-3)]
c   nd(2)    abs of gradient of upspin/downspin density (n) [bohr**(-4)]
c
c output
c   fx       exchange energy density [hartree], 
c               Note: total exchange Ex = Integrate[fx]
c   fc       correlation energy density [hartree]
c               Note: total correlation Ec = Integrate[fc]
c
c Citation request: when using this functional, please cite:
c "R. Armiento and A. E. Mattsson, PRB 72, 085108 (2005);
c  R. Armiento and A. E. Mattsson (unpublished)."
c
c (The first paper for the AM05 functional, the second for the
c spin-polarized version)
c
c Copyright (c) 2005-2008, Rickard Armiento
c
c Permission is hereby granted, free of charge, to any person obtaining 
c a copy of this software and associated documentation files (the 
c "Software"), to deal in the Software without restriction, including 
c without limitation the rights to use, copy, modify, merge, publish, 
c distribute, sublicense, and/or sell copies of the Software, and to 
c permit persons to whom the Software is furnished to do so, subject 
c to the following conditions:
c
c The above copyright notice and this permission notice shall be 
c included in all copies or substantial portions of the Software.
c
c THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
c EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
c OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
c NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
c HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
c WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
c FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
c OTHER DEALINGS IN THE SOFTWARE.
c
c**********************************************************************

      subroutine am05xscss(n,nd,fx,fc)
      implicit none

c     ** Input parameters
      real*8 n(2), nd(2)

c     ** Output parameters
      real*8 fx, fc

c     ** Constants and shorthand notation
      real*8 pi, g, a, si, s2, ni, c
      parameter (pi = 3.141592653589793238462643383279502884197d0)
      parameter (g = 0.8098d0, a = 2.804d0)
      parameter (c = 0.7168d0)

c     ** Local variables
      real*8 exlda, vxlda, eclda, vclda(2)
      real*8 Xs, Hx, Hc, F, denom, zfac, zosn, w

      integer i

      fx = 0.0d0
      fc = 0.0d0

c     *******************
c       LDA correlation
c     *******************
      call am05xscss_ldapwc(n,eclda,vclda)

c     ** Loop over spin up and down
      do 10, i = 1,2

c        ** Cutoff 
         if(n(i) .le. 1.0d-16) then
c        ** the n->0 limit of correlation is not zero.
            fc = fc + n(i)*eclda*g
            go to 10
         endif

c        ** Scaling density and gradient
c        ** Ex = 0.5 * (Ex[2*spin up n] + Ex[2*spin down n] ) 
         ni = 2.0d0*n(i)
         si = nd(i)/(2d0*(3d0*pi**2*2d0*n(i))**(1.0d0/3.0d0)*n(i))
         s2 = si**2

c        *******************
c            LDA exchange
c        *******************
         call am05xscss_ldax(ni,exlda,vxlda)
      
c        ********************
c            Exchange
c        ********************

c        ** Scaled interpolation index
         Xs = 1.0d0/(1.0d0 + a*s2)
      
c        ** Airy LAA refinement function
         call am05xscss_lambertw(si**(3.0d0/2.0d0)/sqrt(24.0d0),w)

c        ** am05_lambertw give back argument if it is < 1.0e-20
c        ** (1.0e-14)^{3/2} = 1.0e-21 => give  low s limit for z/s
c        ** zosn = normalized z/s
         if (si < 1.0e-14) then
            zosn = 1.0d0
         else
            zosn = 24.0d0**(1.0d0/3.0d0)*w**(2.0d0/3.0d0)/si
         end if
         zfac = s2*(zosn*27.0d0/32.0d0/pi**2)**2

c        ** denom = denominator of Airy LAA refinement function
         denom = 1.0d0 + c*s2*zosn*(1.0d0 + zfac)**(1.0d0/4.0d0)
         F = (c*s2 + 1.0d0)/denom
      
c        ** Exchange refinement function
         Hx = Xs + (1.0d0 - Xs)*F

c        ** Exchange energy density, Ex = Integrate[fx]
         fx = fx + 0.5d0*ni*exlda*Hx
      
c        ********************
c            Correlation
c        ********************

c        ** Correlation refinement function 
         Hc = Xs + g*(1.0d0 - Xs)
      
c        ** Correlation energy density, Ec = Integrate[fc]
         fc = fc + 0.5d0*ni*eclda*Hc

 10   continue

      return
      end

c     ******************************************
c       Local density approximation exchange
c
c       input
c       n        electron density [bohr**(-3)]
c
c       output
c       ex       exchange energy per electron [hartree]
c       vx       exchange potential [hartree]
c
c       Copyright (c) 2005, Rickard Armiento
c     ******************************************

      subroutine am05xscss_ldax(n,ex,vx)
c     ** Input parameters
      real*8 n

c     ** Output parameters
      real*8 ex, vx

c     ** Constants
      real*8 pi
      parameter (pi = 3.141592653589793238462643383279502884197d0)

      vx = -(3.0d0*n/pi)**(1.0d0/3.0d0)
      ex = (3.0d0/4.0d0*vx)

      return
      end

c     ***********************************************
c       Local density approximation correlation
c
c       input
c       n(2)     electron upspin/downspin density [bohr**(-3)]
c
c       output
c       ec       correlation energy per electron [hartree]
c       vc(2)    correlation upspin/downspin potential [hartree]
c
c       As parameterized by Perdew Wang,
c         Phys. Rev. B 45, 13244 (1992) 
c       Based on Monte Carlo data by Ceperley Alder, 
c         Phys. Rev. Lett. 45, 566 (1980)
c
c       (Clean room implementation from paper)
c
c       Copyright (c) 2005, Rickard Armiento
c     ***********************************************
      subroutine am05xscss_ldapwc(n,ec,vc)
      implicit none
c     ** Input parameters
      real*8 n(2)

c     ** Output parameters
      real*8 ec, vc(2)

c     ** Constants
      real*8 pi
      real*8 A0,a01,b01,b02,b03,b04
      real*8 A1,a11,b11,b12,b13,b14
      real*8 Aa,aa1,ba1,ba2,ba3,ba4
      parameter (pi = 3.141592653589793238462643383279502884197d0)
      parameter (a01 = 0.21370d0)
      parameter (b01 = 7.5957d0)
      parameter (b02 = 3.5876d0)
      parameter (b03 = 1.6382d0)
      parameter (b04 = 0.49294d0)
      parameter (a11 = 0.20548d0)
      parameter (b11 = 14.1189d0)
      parameter (b12 = 6.1977d0)
      parameter (b13 = 3.3662d0)
      parameter (b14 = 0.62517d0)
      parameter (aa1 = 0.11125d0)
      parameter (ba1 = 10.357d0)
      parameter (ba2 = 3.6231d0)
      parameter (ba3 = 0.88026d0)
      parameter (ba4 = 0.49671d0)
c     ** Paper actually use this:
c      parameter (A0 = 0.031091d0)
c      parameter (A1 = 0.015545d0)
c      parameter (Aa = 0.016887d0)
c     ** But routines now "defacto standard" was distributed using:
      parameter (A0 = 0.0310907d0)
      parameter (A1 = 0.01554535d0)
      parameter (Aa = 0.0168869d0)

c     ** Local variables
      real*8 xi, rsq, f, fp, fb0, mac, ec0, ec1, ecrs, ecxi
      real*8 Q0, Q1, Q1p, ec0rs, ec1rs, acrs, fdenom

c     ** Actual values from paper
c     fdenom = (2.0d0**(4.0d0/3.0d0)-2.0d0)
c     fb0 = 4.0d0/(9.0d0*(2.0d0**(1.0d0/3.0d0)-1.0d0))
c     ** Replaced with "defacto standard" approximations
      fdenom = 0.5198421d0
      fb0 = 1.709921d0

      xi = (n(1)-n(2))/(n(1)+n(2))
      rsq = (3.0d0/(4.0d0*pi*(n(1)+n(2))))**(1.0d0/6.0d0)
      f = ((1.0d0+xi)**(4.0d0/3.0d0)+(1.0d0-xi)**(4.0d0/3.0d0)-2.0d0)/
     *     fdenom

      mac = -2.0d0*Aa*(1.0d0 + aa1*rsq**2)* 
     *     log(1.0d0 + 1.0d0/
     *     (2.0d0*Aa*rsq*(ba1 + rsq*(ba2 + rsq*(ba3 + ba4*rsq)))))

      ec0 = -2.0d0*A0*(1.0d0 + a01*rsq**2)* 
     *     log(1.0d0 + 1.0d0/
     *     (2.0d0*A0*rsq*(b01 + rsq*(b02 + rsq*(b03 + b04*rsq)))))

      ec1 = -2.0d0*A1*(1.0d0 + a11*rsq**2)* 
     *     log(1.0d0 + 1.0d0/
     *     (2.0d0*A1*rsq*(b11 + rsq*(b12 + rsq*(b13 + b14*rsq)))))

      ec = ec0 - mac*f/fb0*(1.0d0-xi**4) + (ec1-ec0)*f*xi**4

      Q0 = -2.0d0*A0*(1.0d0 + a01*rsq**2)
      Q1 = 2.0d0*A0*rsq*(b01 + rsq*(b02 + rsq*(b03 + b04*rsq)))
      Q1p = A0*(b01/rsq+2.0d0*b02+3.0d0*b03*rsq+4.0d0*b04*rsq**2)
      ec0rs = -2.0d0*A0*a01*log(1.0d0 + 1.0d0/Q1)-Q0*Q1p/(Q1**2+Q1)

      Q0 = -2.0d0*A1*(1.0d0 + a11*rsq**2)
      Q1 = 2.0d0*A1*rsq*(b11 + rsq*(b12 + rsq*(b13 + b14*rsq)))
      Q1p = A1*(b11/rsq+2.0d0*b12+3.0d0*b13*rsq+4.0d0*b14*rsq**2)
      ec1rs = -2.0d0*A1*a11*log(1.0d0 + 1.0d0/Q1)-Q0*Q1p/(Q1**2+Q1)

      Q0 = -2.0d0*Aa*(1.0d0 + aa1*rsq**2)
      Q1 = 2.0d0*Aa*rsq*(ba1 + rsq*(ba2 + rsq*(ba3 + ba4*rsq)))
      Q1p = Aa*(ba1/rsq+2.0d0*ba2+3.0d0*ba3*rsq+4.0d0*ba4*rsq**2)
      acrs = -2.0d0*Aa*aa1*log(1.0d0 + 1.0d0/Q1)-Q0*Q1p/(Q1**2+Q1)

      ecrs = ec0rs*(1.0d0 - f*xi**4) + 
     *     ec1rs*f*xi**4 - acrs*f/fb0*(1.0d0 - xi**4)
      fp = 4.0d0/3.0d0*((1.0d0+xi)**(1.0d0/3.0d0) - 
     *     (1.0d0-xi)**(1.0d0/3.0d0))/fdenom
      ecxi = 4.0d0*xi**3*f*(ec1-ec0+mac/fb0)+fp*(xi**4*ec1-xi**4*ec0+
     *     (1.0d0-xi**4)*(-mac)/fb0)

      vc(1)=ec - rsq**2/3.0d0*ecrs - (xi-1.0d0)*ecxi
      vc(2)=ec - rsq**2/3.0d0*ecrs - (xi+1.0d0)*ecxi
      
      end


c     ***********************************************
c       LambertW function. 
c
c       Corless, Gonnet, Hare, Jeffrey, and Knuth (1996), 
c         Adv. in Comp. Math. 5(4):329-359. 
c       Implementation approach loosely inspired by the 
c       GNU Octave version by N. N. Schraudolph, but this 
c       implementation is only for real values and 
c       principal branch.
c
c       Copyright (c) 2005, Rickard Armiento
c     ***********************************************

      subroutine am05xscss_lambertw(z,result)
      implicit none

c     input
      real*8 z
c     output
      real*8 result
c     local variables
      real*8 e,t,p    
      integer i

c     ** If z too low, go with the first term of the power expansion, z
      if( z .lt. 1.0d-20) then
         result = z
         return
      endif

      e = exp(1.0d0)

c     ** Inital guess
      if( abs(z + 1.0d0/e) .gt. 1.45d0 ) then
c        ** Asymptotic expansion at 0 and Inf
         result = log(z)
         result = result - log(result)
      else
c        ** Series expansion about -1/e to first order
         result = 1.0d0*sqrt(2.0d0*e*z + 2.0d0) - 1.0d0
      endif

c     ** Find result through iteration
      do i=1,10
         p = exp(result)
         t = result*p - z
         if( result .ne. -1.0d0 ) then
            t = t/(p*(result + 1.0d0) - 
     *           0.5d0*(result + 2.0d0)*t/(result + 1.0d0))
         else
            t = 0.0d0
         endif
         result = result - t
         if(abs(t) < (2.48d0*1.0d-14)*(1.0d0 + abs(result))) then
            return
         endif
      enddo

c     ** This should never happen!;
      write(*,*) 'am05xscss_lambertw: iteration limit reached.' 
      write(*,*) 'Should never happen: execution aborted.'
      stop
      return
      end

